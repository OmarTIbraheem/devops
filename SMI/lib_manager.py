import os

libs = []
find_text = '#include'
allowed_file_extentions = ['cpp', 'h']
with os.scandir('build') as directory:
    for item in directory:
        if not item.name.startswith('.') and item.is_file():
            file_ext = item.name.split('.')[1]
            if file_ext in allowed_file_extentions:
                with open(item, mode = 'r') as file:
                    lines = file.readlines()
                    for line in lines:
                        if find_text in line:
                            lib = line.split()[1]
                            if lib.startswith('<'):
                                lib = (lib.split('<')[1]).split('>')[0]
                            elif lib.startswith('"'):
                                lib = lib.split('"')[1]
                            if lib.endswith('h'):
                                libs.append(lib)
    with open('libs_list.txt', mode = 'w') as file:
        for lib in libs:
            file.write(lib + '\n')