import os

allowed_file_extentions = ['cpp']
find_text = '#include'
first = False

with os.scandir('logger') as directory:
    for item in directory:
        if not item.name.startswith('.') and item.is_file():
            file_ext = item.name.split('.')[1]
            if file_ext in allowed_file_extentions:
                with open(item, mode = 'r') as file:
                    contents = file.readlines()
                    count = 0
                    for num, line in enumerate(contents, 0):
                        if find_text in line:
                            count += 1
                            if not first:
                                pos1 = num
                                first = True
                pos2 = pos1 + count + 1
                contents.insert(pos1, '#ifndef __clang_analyzer__\n')
                contents.insert(pos2, '#endif\n')
                with open(item, mode = 'w') as file:
                    file.writelines(contents)